import { Workout } from "@/types";

import data from "../../data.json";
import { getStorage, hasKey, removeStorage, storeStorage } from "./storage";

const initWorkouts = async (): Promise<boolean> => {
  const hasWorkout = await hasKey("workouts");
  if (!hasWorkout) {
    await storeStorage("workouts", data);
    return true;
  }

  return false;
};

const getWorkouts = async (): Promise<Workout[]> => {
  const workouts = await getStorage("workouts");
  return workouts;
};

const deleteWorkouts = async (): Promise<boolean> => {
  await removeStorage("workouts");
  return true;
};

export { getWorkouts, initWorkouts, deleteWorkouts };
