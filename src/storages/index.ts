export { storeStorage, getStorage, hasKey, removeStorage } from "./storage";
export { getWorkouts, initWorkouts, deleteWorkouts } from "./workout";
export { setLang, getLang, deleteLang } from "./lang";
