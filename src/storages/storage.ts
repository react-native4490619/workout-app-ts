import AsyncStorage from "@react-native-async-storage/async-storage";

const storeStorage = async (key: string, value: any) => {
  try {
    const stringVal = JSON.stringify(value);
    await AsyncStorage.setItem(key, stringVal);
  } catch (error) {
    console.warn(error);
  }
};

const getStorage = async (key: string) => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (!value) return null;

    const data = JSON.parse(value);
    return data;
  } catch (error) {
    console.warn(error);
  }
};

const hasKey = async (key: string) => {
  try {
    const keys = await AsyncStorage.getAllKeys();
    return keys.includes(key);
  } catch (error) {
    console.warn(error);
  }
};

const removeStorage = async (key: string) => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (error) {
    console.warn(error);
  }
};

export { storeStorage, getStorage, hasKey, removeStorage };
