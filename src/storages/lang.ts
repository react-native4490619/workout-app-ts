import { getStorage, removeStorage, storeStorage } from "./storage";

const setLang = async (data: string): Promise<boolean> => {
  const lang = await storeStorage("lang", data);
  return true;
};

const getLang = async (): Promise<string> => {
  const lang = await getStorage("lang");
  return lang;
};

const deleteLang = async (): Promise<boolean> => {
  await removeStorage("lang");
  return true;
};

export { setLang, getLang, deleteLang };
