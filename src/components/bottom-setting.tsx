import { View, TouchableOpacity, Pressable } from "react-native";
import React, { forwardRef, useMemo } from "react";
import BottomSheet, {
  BottomSheetBackdrop,
  BottomSheetBackgroundProps,
} from "@gorhom/bottom-sheet";

import { i18next, languageStorage } from "@/i18n";
import { FlatList } from "react-native-gesture-handler";
import { Text } from "./text";
import { useTranslation } from "react-i18next";
import { capitalizeFirstSentence } from "@/libs";

interface BottomSettingProps {
  onHandleChangeLang: (lang: string) => void;
}

const renderBackdrop = (props: BottomSheetBackgroundProps) => {
  return (
    <BottomSheetBackdrop
      appearsOnIndex={0}
      disappearsOnIndex={-1}
      {...props}></BottomSheetBackdrop>
  );
};

const RadioIcon = ({ selected }: { selected: boolean }) => (
  <View
    style={{
      width: 20,
      height: 20,
      borderRadius: 10,
      borderWidth: 2,
      borderColor: selected ? "black" : "gray",
      justifyContent: "center",
      alignItems: "center",
    }}>
    {selected && (
      <View
        style={{
          width: 10,
          height: 10,
          borderRadius: 5,
          backgroundColor: "black",
        }}
      />
    )}
  </View>
);

const BottomSetting = forwardRef<BottomSheet, BottomSettingProps>(
  ({ onHandleChangeLang }, ref) => {
    const snapPoints = useMemo(() => ["25%"], []);
    const { t } = useTranslation();

    return (
      <BottomSheet
        ref={ref}
        index={-1}
        snapPoints={snapPoints}
        backdropComponent={renderBackdrop}
        enablePanDownToClose={true}>
        <View
          style={{
            paddingHorizontal: 16,
          }}>
          <Text
            fontWeight='Bold'
            style={{ fontSize: 16, marginBottom: 16 }}>
            {capitalizeFirstSentence(t("changeLang"))}
          </Text>
          <FlatList
            data={Object.values(languageStorage)}
            renderItem={({ item }) => {
              return (
                <View
                  style={{
                    flex: 1,
                    flexDirection: "row",
                    gap: 8,
                    marginBottom: 8,
                    justifyContent: "space-between",
                  }}>
                  <Text>{item.name}</Text>
                  <Pressable onPress={() => onHandleChangeLang(item.code)}>
                    <RadioIcon selected={item.code === i18next.language} />
                  </Pressable>
                </View>
              );
            }}
          />
        </View>
      </BottomSheet>
    );
  },
);

export { BottomSetting };
