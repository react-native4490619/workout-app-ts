import { TextProps as RNTextProps, Text as RNText } from "react-native";

const useCachedFontStyle = () => {
  const getFontStyle = (
    weight: "Light" | "Regular" | "Medium" | "SemiBold" | "Bold" | "ExtraBold",
  ): TextProps["style"] => ({
    fontFamily: `PlusJakartaSans-${weight}`,
  });

  return getFontStyle;
};

interface TextProps extends RNTextProps {
  fontWeight?:
    | "Light"
    | "Regular"
    | "Medium"
    | "SemiBold"
    | "Bold"
    | "ExtraBold";
}

const Text = ({
  children,
  style,
  fontWeight = "Regular",
  ...props
}: TextProps) => {
  const getFontStyle = useCachedFontStyle();

  return (
    <RNText
      style={[getFontStyle(fontWeight), style]}
      {...props}>
      {children}
    </RNText>
  );
};

export { Text };
