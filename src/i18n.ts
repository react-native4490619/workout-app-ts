import i18next from "i18next";
import { initReactI18next } from "react-i18next";
import en from "./locales/en.json";
import id from "./locales/id.json";
import { getLang } from "./storages";

const languageStorage = {
  en: { translation: en, name: "🇬🇧 English", code: "en" },
  id: { translation: id, name: "🇮🇩 Indonesia", code: "id" },
};

const initializeI18n = async () => {
  try {
    const savedLanguage = await getLang();
    const initialLanguage = savedLanguage || "en";

    await i18next.use(initReactI18next).init({
      compatibilityJSON: "v3",
      lng: initialLanguage,
      fallbackLng: "en",
      resources: languageStorage,
    });
  } catch (error) {
    console.error("Error initializing i18next:", error);
    // Tangani kesalahan inisialisasi
  }
};

initializeI18n();

export { languageStorage, i18next };
