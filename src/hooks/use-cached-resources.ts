import { useEffect, useState } from "react";
import * as Font from "expo-font";
import { TextProps } from "react-native";
import { initWorkouts, removeStorage } from "@/storages";

const loadFonts = async () => {
  try {
    await Font.loadAsync({
      "PlusJakartaSans-Light": require("../../assets/fonts/PlusJakartaSans-Light.ttf"),
      "PlusJakartaSans-Regular": require("../../assets/fonts/PlusJakartaSans-Regular.ttf"),
      "PlusJakartaSans-Medium": require("../../assets/fonts/PlusJakartaSans-Medium.ttf"),
      "PlusJakartaSans-SemiBold": require("../../assets/fonts/PlusJakartaSans-SemiBold.ttf"),
      "PlusJakartaSans-Bold": require("../../assets/fonts/PlusJakartaSans-Bold.ttf"),
      "PlusJakartaSans-ExtraBold": require("../../assets/fonts/PlusJakartaSans-ExtraBold.ttf"),
    });
    return true;
  } catch (error) {
    console.warn("Error loading fonts:", error);
    return false;
  }
};

const useCachedResources = () => {
  const [isLoaded, setIsLoaded] = useState<boolean>(false);

  useEffect(() => {
    const load = async () => {
      await initWorkouts();
      const fontsLoaded = await loadFonts();
      setIsLoaded(fontsLoaded);
    };

    load();
  }, [isLoaded]);

  return {
    isLoaded,
    getFontStyle: (
      weight:
        | "Light"
        | "Regular"
        | "Medium"
        | "SemiBold"
        | "Bold"
        | "ExtraBold",
    ): TextProps["style"] => ({
      fontFamily: `PlusJakartaSans-${weight}`,
    }),
  };
};

export { useCachedResources };
