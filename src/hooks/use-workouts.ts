import { getWorkouts } from "@/storages";
import { Workout } from "@/types";
import { useEffect, useState } from "react";
import { useIsFocused } from "@react-navigation/native";

const useWorkouts = () => {
  const [workouts, setWorkouts] = useState<Workout[]>();
  const isFocused = useIsFocused();

  useEffect(() => {
    const getData = async () => {
      const _workouts = await getWorkouts();
      setWorkouts(_workouts);
    };

    if (isFocused) getData();
  }, [isFocused]);

  return workouts;
};

export { useWorkouts };
