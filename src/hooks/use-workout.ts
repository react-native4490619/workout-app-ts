import { getWorkouts } from "@/storages";
import { Workout } from "@/types";
import { useEffect, useState } from "react";
import { useIsFocused } from "@react-navigation/native";

const useWorkout = (slug: string) => {
  const [workout, setWorkout] = useState<Workout>();
  const isFocused = useIsFocused();

  useEffect(() => {
    const getData = async () => {
      const _workout =
        (await getWorkouts()).filter((value) => value.slug === slug)?.[0] ?? {};

      setWorkout(_workout);
    };

    if (isFocused) getData();
  }, [isFocused]);

  return workout;
};

export { useWorkout };
