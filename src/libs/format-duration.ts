const formatDuration = (seconds: number) => {
  const mins = Math.floor(seconds / 60);
  const secs = seconds % 60;
  const minsStr = mins > 0 ? `${mins} min${mins > 1 ? "s" : ""}` : "";
  const secsStr = secs > 0 ? `${secs} sec${secs > 1 ? "s" : ""}` : "";
  return `${minsStr} ${minsStr && secsStr ? " " : ""}${secsStr}`;
};

export { formatDuration };
