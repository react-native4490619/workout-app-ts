type Difficulty = "easy" | "medium" | "hard";
type Type = "exercise" | "break";

interface ISequence {
  slug: string;
  name: string;
  type: Type;
  reps?: number;
  duration: number;
}

interface IWorkout {
  slug: string;
  name: string;
  image: string;
  duration: number;
  difficulty: Difficulty;
  sequence: ISequence[];
}

export type { IWorkout, ISequence };
