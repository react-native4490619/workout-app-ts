import React, { useEffect, useState } from "react";
import {
  View,
  SafeAreaView,
  ImageBackground,
  StyleSheet,
  FlatList,
} from "react-native";
import type { NativeStackScreenProps } from "@react-navigation/native-stack";
import { RouterParams } from "@/navigations";
import { Text } from "@/components";
import { useWorkout } from "@/hooks";
import { ButtonPlay, ListSequenceItem } from "./components";
import { useTranslation } from "react-i18next";

type IndexScreenProps = NativeStackScreenProps<RouterParams, "detail">;

const Index = ({ route }: IndexScreenProps) => {
  const { workoutSlug } = route.params;
  const workout = useWorkout(workoutSlug);
  const [activeSequenceIndex, setActiveSequenceIndex] = useState<number | null>(
    null,
  );
  const [isPlaying, setIsPlaying] = useState<boolean>(false);
  const [remainingTime, setRemainingTime] = useState<number | null>(null);
  const { t } = useTranslation();

  useEffect(() => {
    let timer: NodeJS.Timeout;

    if (isPlaying && activeSequenceIndex !== null && workout) {
      const currentSequence = workout.sequence[activeSequenceIndex];

      if (remainingTime === null) {
        setRemainingTime(currentSequence.duration * 1000);
      }

      timer = setInterval(() => {
        setRemainingTime((prevRemainingTime) => {
          if (prevRemainingTime === null) return null; // Menangani prevRemainingTime yang mungkin null
          const newRemainingTime = prevRemainingTime - 1000; // Mengurangi 1 detik dari remaining time
          if (newRemainingTime <= 0) {
            clearInterval(timer);
            handleSequenceComplete();
            return 0;
          } else {
            return newRemainingTime;
          }
        });
      }, 1000);
    }

    return () => clearInterval(timer);
  }, [isPlaying, activeSequenceIndex, workout, remainingTime]);

  const handlePlayPress = () => {
    if (isPlaying) {
      setIsPlaying(false);
    } else {
      setIsPlaying(true);
      if (activeSequenceIndex === null) {
        setActiveSequenceIndex(0);
      }
    }
  };

  const handleSequenceComplete = () => {
    if (activeSequenceIndex !== null && workout) {
      if (activeSequenceIndex < workout.sequence.length - 1) {
        setActiveSequenceIndex(activeSequenceIndex + 1);
        setRemainingTime(null);
      } else {
        setActiveSequenceIndex(null);
        setIsPlaying(false); // Set isPlaying menjadi false saat semua urutan selesai
      }
    }
  };

  if (!workout) return null;

  return (
    <View style={{ flex: 1 }}>
      <ImageBackground
        source={require("../../../assets/pt.jpg")}
        imageStyle={{ objectFit: "cover" }}
        style={{ flex: 1 }}>
        <View style={styles.overlay}></View>
        <SafeAreaView
          style={{
            flex: 1,
            alignItems: "center",
          }}>
          <View style={styles.containerTitle}>
            <View style={styles.containerDifficulty}>
              <Text style={{ color: "white" }}>
                {workout.sequence.length} total
              </Text>
            </View>
            <Text
              style={{ color: "white", fontSize: 24 }}
              fontWeight='ExtraBold'>
              {workout.name}
            </Text>
          </View>
        </SafeAreaView>
        <View
          style={{
            flex: 4,
            backgroundColor: "white",
            width: "100%",
            borderTopLeftRadius: 32,
            borderTopRightRadius: 32,
            padding: 16,
            gap: 32,
          }}>
          <Text
            style={{ textAlign: "center", fontSize: 16, color: "#64748b" }}
            fontWeight='Medium'>
            {t("detailDesc")}
          </Text>
          <FlatList
            data={workout.sequence}
            keyExtractor={(item) => item.slug}
            renderItem={({ item, index }) => (
              <ListSequenceItem
                item={item}
                isPlay={activeSequenceIndex === index && isPlaying}
                remainingTime={
                  activeSequenceIndex === index ? remainingTime : null
                }
                onComplete={handleSequenceComplete}
              />
            )}
          />
          <ButtonPlay
            onPressHandle={handlePlayPress}
            label={isPlaying ? t("pause") : t("startWorkout")}
          />
        </View>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  overlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: "rgba(0, 0, 0, 0.4)",
  },
  containerDifficulty: {
    borderColor: "white",
    borderWidth: 0.5,
    flexShrink: 1,
    alignSelf: "center",
    paddingHorizontal: 6,
    paddingVertical: 3,
    borderRadius: 5,
  },
  containerTitle: {
    flexShrink: 1,
    gap: 16,
    marginTop: 24,
  },
});

export { Index };
