import { View, TouchableOpacity } from "react-native";
import React from "react";
import { Text } from "@/components";

interface ButtonPlayProps {
  onPressHandle: () => void;
  label: string;
}
const ButtonPlay = ({ onPressHandle, label }: ButtonPlayProps) => {
  return (
    <TouchableOpacity
      onPress={onPressHandle}
      activeOpacity={0.7}>
      <View
        style={{
          backgroundColor: "black",
          marginBottom: 16,
          borderRadius: 10,
          height: 46,
          justifyContent: "center",
          alignItems: "center",
        }}>
        <Text
          style={{
            color: "white",
            fontSize: 16,
          }}
          fontWeight='Medium'>
          {label}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export { ButtonPlay };
