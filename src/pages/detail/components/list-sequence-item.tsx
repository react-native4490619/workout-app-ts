import React, { useEffect, useRef, useState } from "react";
import { View } from "react-native";
import { Sequence } from "@/types";
import { capitalizeFirstSentence, formatDuration } from "@/libs";
import { Text } from "@/components";
import { Play, Pause, Repeat, Timer } from "lucide-react-native";

interface ListSequenceItemProps {
  item: Sequence;
  isPlay?: boolean;
  remainingTime?: number | null;
  onComplete: () => void;
}

const ListSequenceItem = ({
  item,
  isPlay = false,
  remainingTime,
  onComplete,
}: ListSequenceItemProps) => {
  const [countdown, setCountdown] = useState(item.duration);
  const intervalRef = useRef<NodeJS.Timeout | null>(null);

  useEffect(() => {
    if (remainingTime && remainingTime !== null) {
      setCountdown(Math.ceil(remainingTime / 1000));
    } else {
      setCountdown(item.duration);
    }
  }, [remainingTime, item]);

  const startCountdown = () => {
    if (intervalRef.current) return;

    intervalRef.current = setInterval(() => {
      setCountdown((prevCountdown) => {
        if (prevCountdown > 1) {
          return prevCountdown - 1;
        } else {
          clearInterval(intervalRef.current!);
          intervalRef.current = null;
          onComplete();
          return 0;
        }
      });
    }, 1000);
  };

  const stopCountdown = () => {
    if (intervalRef.current) {
      clearInterval(intervalRef.current);
      intervalRef.current = null;
    }
  };

  useEffect(() => {
    if (isPlay) {
      startCountdown();
    } else {
      stopCountdown();
    }

    return stopCountdown;
  }, [isPlay]);

  useEffect(() => {
    return () => {
      if (intervalRef.current) {
        clearInterval(intervalRef.current);
      }
    };
  }, []);

  const minutes = Math.floor(countdown / 60);
  const seconds = countdown % 60;
  const countdownText = `${minutes}:${seconds < 10 ? "0" : ""}${seconds}`;

  return (
    <View
      style={{
        borderRadius: 10,
        width: "auto",
        backgroundColor: "#f1f5f9",
        marginBottom: 16,
        padding: 8,
        flexDirection: "row",
        gap: 24,
        alignItems: "center",
      }}>
      <View
        style={{
          backgroundColor: isPlay ? "white" : "black",
          justifyContent: "center",
          alignItems: "center",
          padding: 8,
          height: 54,
          width: 54,
          borderRadius: 20,
          borderColor: "black",
          borderWidth: 2,
          position: "relative",
          overflow: "hidden",
        }}>
        {!isPlay && (
          <Play
            fill='white'
            color='white'
          />
        )}
        {isPlay && (
          <Pause
            fill='black'
            color='black'
          />
        )}
      </View>

      <View
        style={{
          flex: 1,
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
          paddingLeft: 16,
        }}>
        <View
          style={{
            justifyContent: "center",
            alignItems: "flex-start",
            gap: 3,
          }}>
          <View
            style={{
              backgroundColor: "#e5e7eb",
              alignSelf: "flex-start",
              padding: 4,
              borderRadius: 8,
            }}>
            <Text style={{ color: "black" }}>
              {capitalizeFirstSentence(item.type)}
            </Text>
          </View>

          <Text style={{ fontSize: 16, paddingLeft: 4, fontWeight: "bold" }}>
            {item.name}
          </Text>

          <View style={{ flexDirection: "row", gap: 8, marginTop: 4 }}>
            {item.reps && (
              <View
                style={{
                  flexDirection: "row",
                  gap: 2,
                  borderRightWidth: 1,
                  paddingRight: 5,
                  justifyContent: "center",
                  alignItems: "center",
                }}>
                <Repeat
                  color='grey'
                  size={16}
                />
                <Text
                  style={{
                    paddingLeft: 4,
                    marginTop: 4,
                    color: "grey",
                    fontSize: 12,
                  }}>
                  {item.reps} reps
                </Text>
              </View>
            )}
            {item.duration && (
              <View
                style={{
                  flexDirection: "row",
                  gap: 2,
                  justifyContent: "center",
                  alignItems: "center",
                }}>
                <Timer
                  color='grey'
                  size={16}
                />
                <Text
                  style={{
                    paddingLeft: 4,
                    marginTop: 4,
                    color: "grey",
                    fontSize: 12,
                  }}>
                  {formatDuration(item.duration)}
                </Text>
              </View>
            )}
          </View>
        </View>

        {isPlay && (
          <Text style={{ fontSize: 16, fontWeight: "bold" }}>
            {countdownText}
          </Text>
        )}
      </View>
    </View>
  );
};

export { ListSequenceItem };
