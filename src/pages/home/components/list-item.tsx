import {
  StyleSheet,
  TouchableOpacity,
  View,
  ImageBackground,
} from "react-native";
import React from "react";
import { Workout } from "@/types";
import { capitalizeFirstSentence, formatDuration } from "@/libs";
import { Text } from "@/components";
import { useNavigation } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import { RouterParams } from "@/navigations";

interface ListItemProps {
  item: Workout;
}

const ListItem = ({ item }: ListItemProps) => {
  const navigation =
    useNavigation<StackNavigationProp<RouterParams, "detail">>();
  return (
    <TouchableOpacity
      activeOpacity={0.9}
      onPress={() => {
        navigation.navigate("detail", { workoutSlug: item.slug });
      }}>
      <ImageBackground
        source={{ uri: item.image }}
        imageStyle={{ objectFit: "fill" }}
        style={styles.container}>
        <View style={styles.overlay}></View>
        <View style={styles.innerContainer}>
          <View style={styles.containerLeft}>
            <View style={styles.containerDifficulty}>
              <Text style={{ color: "white" }}>
                {capitalizeFirstSentence(item.difficulty)}
              </Text>
            </View>

            <Text
              style={styles.title}
              fontWeight='Bold'>
              {item.name}
            </Text>
          </View>
          <View style={styles.containerRight}>
            <Text style={{ color: "white" }}>
              {formatDuration(item.duration)}
            </Text>
          </View>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    borderRadius: 10,
    width: "100%",
    overflow: "hidden",
    height: 150,
  },
  containerLeft: {
    gap: 16,
    justifyContent: "center",
    alignItems: "center",
  },
  containerRight: {
    gap: 6,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontWeight: "700",
    fontSize: 16,
    color: "white",
  },
  containerDifficulty: {
    borderColor: "white",
    borderWidth: 0.5,
    flexShrink: 1,
    alignSelf: "flex-start",
    paddingHorizontal: 6,
    paddingVertical: 3,
    borderRadius: 5,
  },
  innerContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 20,
  },
  overlay: {
    ...StyleSheet.absoluteFillObject, // Mengisi seluruh ruang ImageBackground
    backgroundColor: "rgba(0, 0, 0, 0.4)", // Warna semi-transparan (hitam dengan opasitas 0.3)
    borderRadius: 10,
  },
});
export { ListItem };
