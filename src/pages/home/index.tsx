import { View, SafeAreaView, FlatList } from "react-native";

import { styles } from "./style";
import { Shadow } from "react-native-shadow-2";
import { ListItem } from "./components/list-item";
import { useWorkouts } from "@/hooks";

const Index = () => {
  const workouts = useWorkouts();

  return (
    <View style={styles.flex}>
      <SafeAreaView style={styles.flex}>
        <View style={[styles.container, styles.flex]}>
          <FlatList
            data={workouts}
            keyExtractor={(item) => item.slug}
            renderItem={({ item }) => {
              return (
                <Shadow
                  offset={[5, 9]}
                  distance={1}
                  startColor='#00000010'
                  style={{ width: "100%" }}
                  containerStyle={{
                    marginVertical: 10,
                    borderRadius: 10,
                  }}>
                  <View style={{ borderRadius: 10, width: "auto" }}>
                    <ListItem item={item} />
                  </View>
                </Shadow>
              );
            }}
          />
        </View>
      </SafeAreaView>
    </View>
  );
};

export { Index };
