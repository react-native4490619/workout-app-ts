import { View, Text } from "react-native";
import React from "react";
import type { NativeStackScreenProps } from "@react-navigation/native-stack";
import { RouterParams } from "@/navigations";

type IndexScreenProps = NativeStackScreenProps<RouterParams, "planner">;
const Index = ({ navigation }: IndexScreenProps) => {
  return (
    <View>
      <Text>Planner</Text>
    </View>
  );
};

export { Index };
