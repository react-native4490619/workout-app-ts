import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { Detail } from "@/pages";
import { BottomTabNavigation } from "./bottom";
import { RouterParams } from ".";
import { TouchableOpacity, View } from "react-native";
import { Text } from "@/components";
import { ChevronLeft } from "lucide-react-native";
import { useNavigation } from "@react-navigation/native";

const Stack = createNativeStackNavigator<RouterParams>();
const StackNavigation = () => {
  const navigation = useNavigation();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name='root'
        component={BottomTabNavigation}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name='detail'
        component={Detail}
        options={{
          headerTransparent: true,
          title: "",
          headerLeft: () => {
            return (
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <View
                  style={{
                    backgroundColor: "black",
                    borderRadius: 10,
                    padding: 2,
                  }}>
                  <ChevronLeft
                    size={40}
                    color='white'
                  />
                </View>
              </TouchableOpacity>
            );
          },
        }}
      />
    </Stack.Navigator>
  );
};

export { StackNavigation };
