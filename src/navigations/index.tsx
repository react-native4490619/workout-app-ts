import { NavigationContainer } from "@react-navigation/native";

import { StackNavigation } from "./stack";

export type RouterParams = {
  root: undefined;
  home: undefined;
  planner: undefined;
  detail: { workoutSlug: string };
};

const Navigation = () => {
  return (
    <NavigationContainer>
      <StackNavigation />
    </NavigationContainer>
  );
};

export default Navigation;
