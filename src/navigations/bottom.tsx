import React, { useMemo, useRef, useState } from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Home, Planner } from "@/pages";
import { RouterParams } from ".";
import BottomSheet from "@gorhom/bottom-sheet";

import {
  Home as IconHome,
  NotebookPen,
  LucideProps,
  Settings,
} from "lucide-react-native";
import { View, TouchableOpacity } from "react-native";
import { BottomSetting, Text } from "@/components";
import { useTranslation } from "react-i18next";
import { capitalizeFirstSentence } from "@/libs";
import { i18next } from "@/i18n";
import { setLang } from "@/storages";

const IconLabel = ({
  label,
  iconComp: IconComponent,
  color,
  size,
}: {
  label: string;
  iconComp: React.ComponentType<LucideProps>;
  color: string;
  size: number;
}) => (
  <View
    style={{
      alignItems: "center",
      gap: 8,
    }}>
    {[
      <IconComponent
        key='icon'
        size={size}
        color={color}
      />,
    ]}
    <Text style={{ color: color, fontSize: 12 }}>{label}</Text>
  </View>
);

const BottomTab = createBottomTabNavigator<RouterParams>();
const BottomTabNavigation = () => {
  const { t } = useTranslation();
  const bottomSheetRef = useRef<BottomSheet>(null);
  const handleOpenBottomSheet = () => {
    bottomSheetRef.current?.snapToIndex(0);
  };

  const handleChangeLang = (lang: string) => {
    i18next.changeLanguage(lang);
    setLang(lang);
    bottomSheetRef.current?.close();
  };

  return (
    <>
      <BottomTab.Navigator
        initialRouteName='home'
        screenOptions={{
          title: "",
          headerRight: () => {
            return (
              <TouchableOpacity
                activeOpacity={0.5}
                onPress={handleOpenBottomSheet}>
                <View
                  style={{
                    padding: 24,
                  }}>
                  <Settings
                    color='black'
                    size={24}
                  />
                </View>
              </TouchableOpacity>
            );
          },
          unmountOnBlur: true,
          tabBarStyle: {
            backgroundColor: "white",
            borderTopLeftRadius: 40,
            borderTopRightRadius: 40,
            paddingVertical: 20,
            height: 91,
          },
          tabBarActiveTintColor: "black",
        }}>
        <BottomTab.Screen
          name='home'
          component={Home}
          options={{
            tabBarLabel: ({ color }) => (
              <IconLabel
                label={capitalizeFirstSentence(t("home"))}
                iconComp={IconHome}
                color={color}
                size={24}
              />
            ),
            tabBarIcon: () => null,
          }}
        />
        <BottomTab.Screen
          name='planner'
          component={Planner}
          options={{
            tabBarLabel: ({ color }) => (
              <IconLabel
                label={capitalizeFirstSentence(t("planner"))}
                iconComp={NotebookPen}
                color={color}
                size={24}
              />
            ),
            tabBarIcon: () => null,
          }}
        />
      </BottomTab.Navigator>
      <BottomSetting
        ref={bottomSheetRef}
        onHandleChangeLang={handleChangeLang}
      />
    </>
  );
};

export { BottomTabNavigation };
