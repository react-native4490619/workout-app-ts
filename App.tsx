import { StatusBar } from "expo-status-bar";
import Navigation from "@/navigations";
import { useCachedResources } from "@/hooks";
import { GestureHandlerRootView } from "react-native-gesture-handler";

export default function App() {
  const isLoaded = useCachedResources();

  if (!isLoaded) return null;

  return (
    <GestureHandlerRootView style={{ flex: 1 }}>
      <StatusBar style='auto' />
      <Navigation />
    </GestureHandlerRootView>
  );
}
